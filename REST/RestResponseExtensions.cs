﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace BloombergTicketToucher.REST
{
    public static class RestResponseExtensions
    {
        public static bool IsSuccess(this IRestResponse response)
        {
            if (response == null)
            {
                return false;
            }
            return response.IsComplete() && response.IsSuccessStatusCode();
        }

        public static bool IsSuccessStatusCode(this IRestResponse response)
        {
            if (response == null)
            {
                return false;
            }
            var codeValue = (int)response.StatusCode;
            return codeValue >= 200
                   && codeValue <= 399;
        }

        public static bool IsComplete(this IRestResponse response)
        {
            if (response == null)
            {
                return false;
            }
            switch (response.ResponseStatus)
            {
                case ResponseStatus.Aborted:
                case ResponseStatus.Error:
                case ResponseStatus.TimedOut:
                case ResponseStatus.None:
                    return false;
                case ResponseStatus.Completed:
                default:
                    return true;
            }
        }
    }
}
