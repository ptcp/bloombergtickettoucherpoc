﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BloombergTicketToucher.Contracts.Enums;

namespace BloombergTicketToucher.Contracts
{
    class SpecialKeys
    {
        public string this[SpecialKey value] => _dict.ContainsKey(value) ? _dict[value] : "";


        public static SpecialKeys Dict { get; }

        static SpecialKeys()
        {
            Dict = new SpecialKeys();
        }

        private static Dictionary<SpecialKey, string> _dict =
            new Dictionary<SpecialKey, string>
            {
                {SpecialKey.Backspace,"{BACKSPACE}"},
                {SpecialKey.Break,"{BREAK}"},
                {SpecialKey.CapsLock,"{CAPSLOCK}"},
                {SpecialKey.Delete,"{DELETE}"},
                {SpecialKey.DownArrow,"{DOWN}"},
                {SpecialKey.End,"{END}"},
                {SpecialKey.Enter,"{ENTER}"},
                {SpecialKey.Esc,"{ESC}"},
                {SpecialKey.Help,"{HELP}"},
                {SpecialKey.Home,"{HOME}"},
                {SpecialKey.Insert,"{INSERT}"},
                {SpecialKey.LeftArrow,"{LEFT}"},
                {SpecialKey.NumLock,"{NUMLOCK}"},
                {SpecialKey.PageDown,"{PGDN}"},
                {SpecialKey.PageUp,"{PGUP}"},
                {SpecialKey.PrintScreen,"{PRTSC}"},
                {SpecialKey.RightArrow,"{RIGHT}"},
                {SpecialKey.ScrollLock,"{SCROLLLOCK}"},
                {SpecialKey.Tab,"{TAB}"},
                {SpecialKey.UpArrow,"{UP}"},
                {SpecialKey.F1,"{F1}"},
                {SpecialKey.F2,"{F2}"},
                {SpecialKey.F3,"{F3}"},
                {SpecialKey.F4,"{F4}"},
                {SpecialKey.F5,"{F5}"},
                {SpecialKey.F6,"{F6}"},
                {SpecialKey.F7,"{F7}"},
                {SpecialKey.F8,"{F8}"},
                {SpecialKey.F9,"{F9}"},
                {SpecialKey.F10,"{F10}"},
                {SpecialKey.F11,"{F11}"},
                {SpecialKey.F12,"{F12}"},
                {SpecialKey.F13,"{F13}"},
                {SpecialKey.F14,"{F14}"},
                {SpecialKey.F15,"{F15}"},
                {SpecialKey.F16,"{F16}"},
                {SpecialKey.KeypadAdd,"{ADD}"},
                {SpecialKey.KeypadSubtract,"{SUBTRACT}"},
                {SpecialKey.KeypadMultiply,"{MULTIPLY}"},
                {SpecialKey.KeypadDivide,"{DIVIDE}"}
            };



    }

        //Special keys can be found at https://docs.microsoft.com/en-us/dotnet/api/system.windows.forms.sendkeys
        //if every needed, extend this class to handle pressing multiple keys
        //Shift - '+' as in "+e" means press shift and e.
        //Control - '^' - "^e" means press control and e
        //Alt - '%' - "%e" means press alt and e

}