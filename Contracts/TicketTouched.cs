﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloombergTicketToucher.Contracts
{
    public class TicketTouchedEventArgs : EventArgs
    {
        public TicketTouchedEventArgs(string ticketNumber)
        {
            TicketNumber = ticketNumber;
        }

        public string TicketNumber { get; }
    }
}
