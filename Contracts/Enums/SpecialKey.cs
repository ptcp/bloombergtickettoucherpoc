﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloombergTicketToucher.Contracts.Enums
{
    public enum SpecialKey
    {
        Backspace,
        Break,
        CapsLock,
        Delete,
        DownArrow,
        End,
        Enter,
        Esc,
        Help,
        Home,
        Insert,
        LeftArrow,
        NumLock,
        PageDown,
        PageUp,
        PrintScreen,
        RightArrow,
        ScrollLock,
        Tab,
        UpArrow,
        F1,
        F2,
        F3,
        F4,
        F5,
        F6,
        F7,
        F8,
        F9,
        F10,
        F11,
        F12,
        F13,
        F14,
        F15,
        F16,
        KeypadAdd,
        KeypadSubtract,
        KeypadMultiply,
        KeypadDivide
    }
}
