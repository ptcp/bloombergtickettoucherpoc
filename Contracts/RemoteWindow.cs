﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using PTrust.EnterpriseLogger.IntegrationExtensions;

namespace BloombergTicketToucher.Contracts
{

    public class RemoteWindow
    {
        [DllImport("User32.dll", CharSet = CharSet.Unicode)]
        static extern int SetForegroundWindow(IntPtr point);


        //these were used on console app most likely get rid of here
        const int PaddingPid = 20;
        const int PaddingMwh = 20;
        const int PaddingPn = 20;
        const int PaddingMwt = 0;

        public Process CurrentProcess { get; private set; }
        public Process RemoteProcess { get; private set; }

        public RemoteWindow(string windowTitleRegEx)
        {
            CurrentProcess = Process.GetCurrentProcess();

            PtLogger.LogInfo($@"Looking for windows with Title that matches RegEx '{windowTitleRegEx}'");

            List<Process> targetWindows = Process.GetProcesses().Where(x => Regex.IsMatch(x.MainWindowTitle, windowTitleRegEx, RegexOptions.IgnoreCase)).ToList();

            if (targetWindows.Count == 0)
            {
                PtLogger.LogInfo("Unable to find windows matching RegEx.");
                RemoteProcess = null;
            }
            else
            {
                PtLogger.LogInfo($"{"PID",-PaddingPid}{"Process Name",-PaddingPn}{"Window Title",-PaddingMwt}");
                foreach (var process in targetWindows)
                {
                    PtLogger.LogInfo($"{process.Id,-PaddingPid}{process.ProcessName,-PaddingPn}{process.MainWindowTitle,-PaddingMwt}");
                }

                RemoteProcess = targetWindows.First();

                if (targetWindows.Count > 1)
                {
                    PtLogger.LogInfo($"Found multiple windows.  Using '{RemoteProcess.MainWindowTitle}', Pid={RemoteProcess.Id}");
                }
                else
                {
                    PtLogger.LogInfo($"Found window, '{RemoteProcess.MainWindowTitle}', Pid={RemoteProcess.Id}");
                }
            }
        }



        public void PressKeys(string keys, int waitTimeSeconds)
        {
            SendKeys.SendWait($"{keys}");
            Thread.Sleep(waitTimeSeconds * 1000);
        }

        public bool SetFocusRemote()
        {
            return SetFocus(RemoteProcess);
        }

        public bool SetFocusCurrent()
        {
            return SetFocus(CurrentProcess);
        }

        bool SetFocus(Process p)
        {
            try
            {
                Interaction.AppActivate(p.Id);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        bool SetFocusAlternate(Process p)
        {
            try
            {
                IntPtr pointer = p.MainWindowHandle;
                SetForegroundWindow(pointer);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }


        bool SetFocus(string title)
        {
            try
            {
                Interaction.AppActivate(title);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }
    }
}