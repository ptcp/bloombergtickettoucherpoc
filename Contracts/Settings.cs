﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace BloombergTicketToucher.Contracts
{
    class Settings
    {
        public static int RestTimeoutInSeconds => int.Parse(ConfigurationManager.AppSettings["RestTimeoutInSeconds"]);
        public static string TradeControlUrl => ConfigurationManager.AppSettings["TradeControlUrl"] ?? string.Empty;
        public static string WindowTitleRegEx => ConfigurationManager.AppSettings["WindowTitleRegEx"] ?? string.Empty;
    }
}
