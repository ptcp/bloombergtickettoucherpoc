﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloombergTicketToucher.Interfaces
{
    public interface ITradeControlQueryService
    {
        List<string> GetTicketsToTouch();
    }
}
