﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BloombergTicketToucher.Contracts;

namespace BloombergTicketToucher.Interfaces
{
    public interface IBloombergController
    {
        event EventHandler<TicketTouchedEventArgs> TicketTouched;

        bool CancelTouch { get; set; }

        bool ActivateRemote(bool stayOnRemote);

        List<string> GetTicketsToTouch();

        void SendTickets();
    }
}
