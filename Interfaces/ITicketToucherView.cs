﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BloombergTicketToucher.Contracts;

namespace BloombergTicketToucher.Interfaces
{
    public interface ITicketToucherView
    {
        RemoteWindow RemoteWindow { get; set; }
        List<string> Tickets { get; set; }
    }
}
