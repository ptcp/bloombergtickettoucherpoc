﻿using System;

namespace TradeControl.Domain.DataContracts.Types.Toms
{
    public sealed class TomsTicket
    {
        public string BloombergSecurityIdentifier { get; set; }
        public int BloombergTransactionNumber { get; set; }
        public string BloombergTransactionType { get; set; }
        public string BloombergYellowKey { get; set; }
        public Guid Id { get; set; }
        public DateTimeOffset? MaturityDate { get; set; }
        public decimal? PrepaymentSpeed { get; set; }
        public string PrepaymentType { get; set; }
        public string SalesBookName { get; set; }
        public string SecurityIdentifier { get; set; }
        public string TradeBookName { get; set; }
        public DateTimeOffset TradeDate { get; set; }
        public decimal TradePrice { get; set; }
        public decimal TradePrincipal { get; set; }
        public decimal TradeQuantity { get; set; }
        public decimal TradeTotal { get; set; }
        public decimal? WeightedAverageLife { get; set; }
        public decimal Yield { get; set; }
        public bool IsCancelledByCorrection { get; set; }
        public bool IsCreatedByFactorUpdate { get; set; }
        public int OriginalBloombergTicketNumber { get; set; }
        public string SalespersonName { get; set; }
        public bool IsSecondHalfOfTraderToTraderTransaction { get; set; }
        public bool IsTraderToTraderTransaction { get; set; }
        public int RelatedSalesTicketId { get; set; }
        public decimal? CouponRate { get; set; }
        public string TradeActionType { get; set; }
        public string Ticker { get; set; }
        public string InboundMessageBloombergReferenceNumber { get; set; }

        public int MasterBloombergTransactionNumber { get; set; }
        public string SalespersonLoginBloombergId { get; set; }
        public DateTimeOffset TransactionDate { get; set; }
        public DateTimeOffset SettlementDate { get; set; }
        public Decimal SalesCreditAmount { get; set; }
        public Decimal SalesCreditRate { get; set; }
        public Decimal? Factor { get; set; }
        public Decimal AccruedInterestAmount { get; set; }
        public string CounterpartyAccountId { get; set; }
        public string SecurityBloombergGlobalId { get; set; }   
        public string BuySellCoverShort { get; set; }
        public string CounterpartyAccountShortName { get; set; }
        public decimal SettlementTotalInSettlementCurrency { get; set; }
    }
}
