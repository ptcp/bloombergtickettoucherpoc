﻿using System;

namespace TradeControl.Domain.DataContracts.Types.Reporting.CouponRecon
{
    public class BbgSecurity
    {
        public string SecurityType2 { get; set; }
        public int? CpnFreq { get; set; }
        public decimal? MtgAccrualRt { get; set; }
        public DateTime? BaseAccrualRtDate { get; set; }
        public decimal? BaseAccrualRt { get; set; }
        public DateTime? BaseAccrualRtDateB1 { get; set; }
        public decimal? BaseAccrualRtB1 { get; set; }
        public DateTime? BaseAccrualRtDateB2 { get; set; }
        public decimal? BaseAccrualRtB2 { get; set; }
        public DateTime? BaseAccrualRtDateB3 { get; set; }
        public decimal? BaseAccrualRtB3 { get; set; }
        public decimal? MtgDealOrigFace { get; set; }
        public string SecurityDescription { get; set; }

    }
}