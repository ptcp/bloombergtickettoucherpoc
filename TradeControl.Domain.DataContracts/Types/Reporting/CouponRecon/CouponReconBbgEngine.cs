﻿using System;
using System.Collections.Generic;
using TradeControl.Domain.DataContracts.Types.Toms;

namespace TradeControl.Domain.DataContracts.Types.Reporting.CouponRecon
{
    public class SapiAsOfResponse
    {
        public string Cusip { get; set; }
        public DateTime? SettleDate { get; set; }
        public int? DaysAccrued { get; set; }
        public decimal? Factor { get; set; }
        public decimal? MtgFactorSettleDate { get; set; }
        public DateTime? AccrualPerStartDate { get; set; }
        public DateTime? AccrualPerEndDate { get; set; }
    }

    public class SapiSecurityResponse
    {
        public string Cusip { get; set; }
        public string SECURITY_TYP2 { get; set; }
        public int? CPN_FREQ { get; set; }
        public decimal? MTG_ACC_RT { get; set; }
        public decimal? BASE_ACC_RT { get; set; }
        public DateTime? BASE_ACC_RT_DT { get; set; }
        public decimal? BASE_ACC_RT_BACK1 { get; set; }
        public DateTime? BASE_ACC_RT_DT_BACK1 { get; set; }
        public decimal? BASE_ACC_RT_BACK2 { get; set; }
        public DateTime? BASE_ACC_RT_DT_BACK2 { get; set; }
        public decimal? BASE_ACC_RT_BACK3 { get; set; }
        public DateTime? BASE_ACC_RT_DT_BACK3 { get; set; }
        public decimal? MTG_DEAL_ORIG_FACE { get; set; }
        public string SECURITY_DESC { get; set; }
    }


    public class SapiAsOFDataResponse
    {
        public List<SapiAsOfResponse> CompleteResponses { get; set; }
        public List<TomsTicket> IncompleteResponses { get; set; }
    }

    public class SapiSecurityDataResponse
    {
        public List<SapiSecurityResponse> CompleteResponses { get; set; }
        public List<TomsTicket> IncompleteResponses { get; set; }
    }

}