﻿namespace TradeControl.Domain.DataContracts.Types.Reporting.CouponRecon
{
    public class CouponTradeAccount
    {
        public string AccountNum { get; set; }
        public int MasterAccountId { get; set; }
        public string LongNm { get; set; }
        public bool IsMasterAccount { get; set; }

        public int? FirmAccountId { get; set; }
        public string FirmAccountTypeCd { get; set; }
    }
}