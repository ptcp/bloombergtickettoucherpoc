﻿using System.Collections.Generic;

namespace TradeControl.Domain.DataContracts.Types.Reporting.CouponRecon
{
    public class SecurityTicketQueryResponse
    {
        public int PageNumber { get; set; }
        public int TotalRecords { get; set; }
        public int TotalPages { get; set; }
        public List<CouponSecurityTicket> Tickets { get; set; }

    }
}