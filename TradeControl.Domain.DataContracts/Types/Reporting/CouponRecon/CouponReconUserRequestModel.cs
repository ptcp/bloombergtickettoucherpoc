﻿using System;

namespace TradeControl.Domain.DataContracts.Types.Reporting.CouponRecon
{
    public class CouponReconUserRequestModel
    {
        //TODO: verify that DateTimeOffset should be nullable,
        //TODO: it seems like there should always be a default
        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public int RecordsPerPage { get; set; }
        public int PageNumber { get; set; }
        public int? TicketId { get; set; }
        public string SecurityId { get; set; }
        public string TradeBook { get; set; }
        public string SortColumn { get; set; }
        public bool SortDescending { get; set; }
        public bool ApprovedFilter { get; set; }


        public string CouponSortColumn { get; set; }
        public bool CouponSortDescending { get; set; }
        public bool AccruedDiff100 { get; set; }
        public bool AccruedDiffFlat { get; set; }
        public bool AccruedAlloDiff100 { get; set; }
        public bool AccruedAlloDiffFlat { get; set; }
        public bool AccruedDiffNull { get; set; }
        public bool AccruedAlloDiffNull { get; set; }


    }


    public static class CouponReconSearchCriteria
    {
        public const string AccruedDifference = "accruedDifference";
        public const string TradeDate = "tradeDate";
        public const string TicketId = "bloombergTransactionNumber";
        public const string TradeBook = "traderAccountName";
        public const string SecurityId = "securityIdentifier";
    }


}