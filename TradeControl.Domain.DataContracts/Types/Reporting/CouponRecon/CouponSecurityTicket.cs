﻿using System;
using System.Collections.Generic;
using TradeControl.Domain.DataContracts.Types.Toms;

namespace TradeControl.Domain.DataContracts.Types.Reporting.CouponRecon
{
    public class CouponSecurityTicket
    {
        //TODO can we move to a model that supports population via constructor, or will we always need direct access to the setter of the properties?
        public CouponSecurityTicket() { }

        public CouponSecurityTicket(TomsTicket ticket,
            BbgSecurity bbgSecurity = null,
            SapiAsOfResponse sapiData = null,
            CouponTradeAccount tradeAccount = null,
            CouponReconComputedValues computedValues = null,
            List<CouponSecurityTicket> childrenTickets = null,
            bool allocationOver100 = true)
        {
            Ticket = ticket;
            Security = bbgSecurity ?? new BbgSecurity();
            SapiData = sapiData ?? new SapiAsOfResponse();
            CouponTradeAccount = tradeAccount ?? new CouponTradeAccount();
            ComputedValues = computedValues ?? new CouponReconComputedValues();
            ChildrenTickets = childrenTickets ?? new List<CouponSecurityTicket>();
            AllocationOver100 = allocationOver100;
        }

        public int BloombergTransactionNumber => Ticket.BloombergTransactionNumber;
        public int MasterAccountId => CouponTradeAccount.MasterAccountId;
        public BbgSecurity Security { get; set; } = new BbgSecurity();
        public TomsTicket Ticket { get; set; }
        public SapiAsOfResponse SapiData { get; set; } = new SapiAsOfResponse();
        public CouponReconComputedValues ComputedValues { get; set; } = new CouponReconComputedValues();
        public CouponTradeAccount CouponTradeAccount { get; set; } = new CouponTradeAccount();
        public List<CouponSecurityTicket> ChildrenTickets { get; set; } = new List<CouponSecurityTicket>();
        public bool AllocationOver100 { get; set; } = true;
    }

    public class CouponTicketCacheResults
    {
        public List<CouponSecurityTicket> ExistingTickets { get; set; }
        public List<DateTimeOffset> MissingTickets { get; set; }
    }



}