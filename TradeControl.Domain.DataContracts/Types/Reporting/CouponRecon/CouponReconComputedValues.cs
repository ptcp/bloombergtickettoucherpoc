﻿namespace TradeControl.Domain.DataContracts.Types.Reporting.CouponRecon
{
    public class CouponReconComputedValues
    {
        public decimal? ComputedTradeRate { get; set; }
        public decimal? ComputedFactor { get; set; }
        public decimal? ComputedIntendedInterestRt { get; set; }
        public decimal? ComputedAccruedDifference { get; set; }
        public bool CheckManuallyBool { get; set; }
        public string CheckManuallyMsg { get; set; }

    }
}