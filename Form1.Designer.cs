﻿namespace BloombergTicketToucher
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonFindWindow = new System.Windows.Forms.Button();
            this.buttonTouchTickets = new System.Windows.Forms.Button();
            this.buttonActivateWindow = new System.Windows.Forms.Button();
            this.buttonGetTickets = new System.Windows.Forms.Button();
            this.labelWindowRegEx = new System.Windows.Forms.Label();
            this.listViewTickets = new System.Windows.Forms.ListView();
            this.textBoxWindowRegEx = new System.Windows.Forms.TextBox();
            this.labelWindowPid = new System.Windows.Forms.Label();
            this.textBoxWindowPid = new System.Windows.Forms.TextBox();
            this.labelWindowTitle = new System.Windows.Forms.Label();
            this.textBoxWindowTitle = new System.Windows.Forms.TextBox();
            this.buttonClearTickets = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonFindWindow
            // 
            this.buttonFindWindow.Location = new System.Drawing.Point(430, 13);
            this.buttonFindWindow.Name = "buttonFindWindow";
            this.buttonFindWindow.Size = new System.Drawing.Size(102, 23);
            this.buttonFindWindow.TabIndex = 0;
            this.buttonFindWindow.Text = "Find Window";
            this.buttonFindWindow.UseVisualStyleBackColor = true;
            this.buttonFindWindow.Click += new System.EventHandler(this.ButtonFindWindow_Click);
            // 
            // buttonTouchTickets
            // 
            this.buttonTouchTickets.Location = new System.Drawing.Point(430, 129);
            this.buttonTouchTickets.Name = "buttonTouchTickets";
            this.buttonTouchTickets.Size = new System.Drawing.Size(102, 23);
            this.buttonTouchTickets.TabIndex = 1;
            this.buttonTouchTickets.Text = "Touch Tickets";
            this.buttonTouchTickets.UseVisualStyleBackColor = true;
            this.buttonTouchTickets.Click += new System.EventHandler(this.ButtonTouchTickets_Click);
            // 
            // buttonActivateWindow
            // 
            this.buttonActivateWindow.Location = new System.Drawing.Point(430, 43);
            this.buttonActivateWindow.Name = "buttonActivateWindow";
            this.buttonActivateWindow.Size = new System.Drawing.Size(102, 23);
            this.buttonActivateWindow.TabIndex = 2;
            this.buttonActivateWindow.Text = "Activate Window";
            this.buttonActivateWindow.UseVisualStyleBackColor = true;
            this.buttonActivateWindow.Click += new System.EventHandler(this.ButtonActivateWindow_Click);
            // 
            // buttonGetTickets
            // 
            this.buttonGetTickets.Location = new System.Drawing.Point(430, 91);
            this.buttonGetTickets.Name = "buttonGetTickets";
            this.buttonGetTickets.Size = new System.Drawing.Size(75, 23);
            this.buttonGetTickets.TabIndex = 3;
            this.buttonGetTickets.Text = "Get Tickets";
            this.buttonGetTickets.UseVisualStyleBackColor = true;
            this.buttonGetTickets.Click += new System.EventHandler(this.ButtonGetTickets_Click);
            // 
            // labelWindowRegEx
            // 
            this.labelWindowRegEx.AutoSize = true;
            this.labelWindowRegEx.Location = new System.Drawing.Point(23, 13);
            this.labelWindowRegEx.Name = "labelWindowRegEx";
            this.labelWindowRegEx.Size = new System.Drawing.Size(184, 13);
            this.labelWindowRegEx.TabIndex = 5;
            this.labelWindowRegEx.Text = "Window to Find (Regular Expression):";
            // 
            // listViewTickets
            // 
            this.listViewTickets.GridLines = true;
            this.listViewTickets.HideSelection = false;
            this.listViewTickets.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.listViewTickets.Location = new System.Drawing.Point(26, 119);
            this.listViewTickets.MaximumSize = new System.Drawing.Size(189, 600);
            this.listViewTickets.MinimumSize = new System.Drawing.Size(189, 97);
            this.listViewTickets.Name = "listViewTickets";
            this.listViewTickets.Size = new System.Drawing.Size(189, 110);
            this.listViewTickets.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listViewTickets.TabIndex = 6;
            this.listViewTickets.UseCompatibleStateImageBehavior = false;
            this.listViewTickets.View = System.Windows.Forms.View.Details;
            // 
            // textBoxWindowRegEx
            // 
            this.textBoxWindowRegEx.Location = new System.Drawing.Point(228, 13);
            this.textBoxWindowRegEx.Name = "textBoxWindowRegEx";
            this.textBoxWindowRegEx.Size = new System.Drawing.Size(149, 20);
            this.textBoxWindowRegEx.TabIndex = 7;
            // 
            // labelWindowPid
            // 
            this.labelWindowPid.AutoSize = true;
            this.labelWindowPid.Location = new System.Drawing.Point(26, 43);
            this.labelWindowPid.Name = "labelWindowPid";
            this.labelWindowPid.Size = new System.Drawing.Size(67, 13);
            this.labelWindowPid.TabIndex = 8;
            this.labelWindowPid.Text = "Window PID";
            // 
            // textBoxWindowPid
            // 
            this.textBoxWindowPid.Enabled = false;
            this.textBoxWindowPid.Location = new System.Drawing.Point(228, 43);
            this.textBoxWindowPid.Name = "textBoxWindowPid";
            this.textBoxWindowPid.Size = new System.Drawing.Size(149, 20);
            this.textBoxWindowPid.TabIndex = 9;
            // 
            // labelWindowTitle
            // 
            this.labelWindowTitle.AutoSize = true;
            this.labelWindowTitle.Location = new System.Drawing.Point(29, 72);
            this.labelWindowTitle.Name = "labelWindowTitle";
            this.labelWindowTitle.Size = new System.Drawing.Size(69, 17);
            this.labelWindowTitle.TabIndex = 10;
            this.labelWindowTitle.Text = "Window Title";
            this.labelWindowTitle.UseCompatibleTextRendering = true;
            // 
            // textBoxWindowTitle
            // 
            this.textBoxWindowTitle.Enabled = false;
            this.textBoxWindowTitle.Location = new System.Drawing.Point(228, 72);
            this.textBoxWindowTitle.Name = "textBoxWindowTitle";
            this.textBoxWindowTitle.Size = new System.Drawing.Size(149, 20);
            this.textBoxWindowTitle.TabIndex = 11;
            // 
            // buttonClearTickets
            // 
            this.buttonClearTickets.Location = new System.Drawing.Point(430, 170);
            this.buttonClearTickets.Name = "buttonClearTickets";
            this.buttonClearTickets.Size = new System.Drawing.Size(102, 23);
            this.buttonClearTickets.TabIndex = 12;
            this.buttonClearTickets.Text = "Clear Ticket List";
            this.buttonClearTickets.UseVisualStyleBackColor = true;
            this.buttonClearTickets.Click += new System.EventHandler(this.ButtonClearTickets_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(605, 316);
            this.Controls.Add(this.buttonClearTickets);
            this.Controls.Add(this.textBoxWindowTitle);
            this.Controls.Add(this.labelWindowTitle);
            this.Controls.Add(this.textBoxWindowPid);
            this.Controls.Add(this.labelWindowPid);
            this.Controls.Add(this.textBoxWindowRegEx);
            this.Controls.Add(this.listViewTickets);
            this.Controls.Add(this.labelWindowRegEx);
            this.Controls.Add(this.buttonGetTickets);
            this.Controls.Add(this.buttonActivateWindow);
            this.Controls.Add(this.buttonTouchTickets);
            this.Controls.Add(this.buttonFindWindow);
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(0, 0, 15, 15);
            this.Text = "Bloomberg Ticket Toucher";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonFindWindow;
        private System.Windows.Forms.Button buttonTouchTickets;
        private System.Windows.Forms.Button buttonActivateWindow;
        private System.Windows.Forms.Button buttonGetTickets;
        private System.Windows.Forms.Label labelWindowRegEx;
        internal System.Windows.Forms.ListView listViewTickets;
        private System.Windows.Forms.TextBox textBoxWindowRegEx;
        private System.Windows.Forms.Label labelWindowPid;
        private System.Windows.Forms.TextBox textBoxWindowPid;
        private System.Windows.Forms.Label labelWindowTitle;
        private System.Windows.Forms.TextBox textBoxWindowTitle;
        private System.Windows.Forms.Button buttonClearTickets;
    }
}

