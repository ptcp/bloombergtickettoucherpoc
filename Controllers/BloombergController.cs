﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

using BloombergTicketToucher.Contracts;
using BloombergTicketToucher.Contracts.Enums;
using BloombergTicketToucher.Interfaces;
using PTrust.EnterpriseLogger.IntegrationExtensions;

namespace BloombergTicketToucher
{
    public class BloombergController : IBloombergController
    {
        private ITicketToucherView _ticketToucherView;
        private ITradeControlQueryService _tradeControlQueryService;

        public event EventHandler<TicketTouchedEventArgs> TicketTouched;

        public bool CancelTouch { get; set; } = false;
        
        public BloombergController(ITicketToucherView ticketToucher, ITradeControlQueryService tradeControlQueryService)
        {
            _ticketToucherView = ticketToucher;
            _tradeControlQueryService = tradeControlQueryService;
        }

        public bool ActivateRemote(bool stayOnRemote = false)
        {
            if (!_ticketToucherView.RemoteWindow.SetFocusRemote())
            {
                PtLogger.LogError($"Unable to activate window, '{_ticketToucherView.RemoteWindow.RemoteProcess.MainWindowTitle}', Pid={_ticketToucherView.RemoteWindow.RemoteProcess.Id}");
                return false;
            }
            else
            {
                PtLogger.LogInfo($"Window activated, '{_ticketToucherView.RemoteWindow.RemoteProcess.MainWindowTitle}', Pid={_ticketToucherView.RemoteWindow.RemoteProcess.Id}");

                Thread.Sleep(500); //wait half a second.  Seems like sometimes it misses switching back to current window
                if (!stayOnRemote)
                {
                    _ticketToucherView.RemoteWindow.SetFocusCurrent();
                }

                return true;
            }
        }

        public List<string> GetTicketsToTouch()
        {
            try
            {
                //Query service does its own logging
                return _tradeControlQueryService.GetTicketsToTouch();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void SendTickets()
        {
            List<string> touchedTickets = new List<string>();
            CancelTouch = false;
            PtLogger.LogInfo($"Start touching tickets ({string.Join(",", _ticketToucherView.Tickets)})");

            //Reset Bloomberg Screen
            _ticketToucherView.RemoteWindow.SetFocusRemote();
            _ticketToucherView.RemoteWindow.PressKeys($"{SpecialKeys.Dict[SpecialKey.Esc]}", 1);

            foreach (string ticket in _ticketToucherView.Tickets)
            {
                SendTicket(ticket);
                touchedTickets.Add(ticket);
                PtLogger.LogInfo($"Touched ticket - {ticket}");
                TicketTouchedEventArgs ticketNumber = new TicketTouchedEventArgs(ticket);
                TicketTouched?.Invoke(this, ticketNumber);
                if (CancelTouch)
                {
                    PtLogger.LogInfo("Cancel requested.  Will not touch remaining tickets.");
                    break;  //keep it simple for now.  If we want more fine grain control switch this to a custom exception type and catch it on the form
                }
            }

            if (_ticketToucherView.Tickets.Count == touchedTickets.Count)
            {
                PtLogger.LogInfo("Successfully touched all tickets");
            }
            else
            {
                List<string> untouchedTickets = _ticketToucherView.Tickets.Except(touchedTickets).ToList();
                PtLogger.LogInfo($"Not all tickets touched, did not touch '{string.Join(",", untouchedTickets)}'");
            }
            
            _ticketToucherView.RemoteWindow.SetFocusCurrent();
        }

        void SendTicket(string ticket)
        {
            _ticketToucherView.RemoteWindow.SetFocusRemote();   //just in case they clicked somewhere
            _ticketToucherView.RemoteWindow.PressKeys($"TBLT {ticket}~", 1);
            _ticketToucherView.RemoteWindow.PressKeys(
                $"{SpecialKeys.Dict[SpecialKey.DownArrow]}{SpecialKeys.Dict[SpecialKey.DownArrow]}{SpecialKeys.Dict[SpecialKey.Enter]}",
                1);
            _ticketToucherView.RemoteWindow.PressKeys($"1 {SpecialKeys.Dict[SpecialKey.Enter]}", 3);
        }
    }
}