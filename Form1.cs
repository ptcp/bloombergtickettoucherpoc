﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

using BloombergTicketToucher.Contracts;
using BloombergTicketToucher.Interfaces;
using BloombergTicketToucher.Query;

namespace BloombergTicketToucher
{
    public partial class Form1 : Form, ITicketToucherView
    {
        private IBloombergController _bloombergController;

        public RemoteWindow RemoteWindow { get; set; }
        public List<string> Tickets { get; set; } = new List<string>();

        private bool foundWindow = false; 

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _bloombergController = new BloombergController(this, new TradeControlQueryService());
            _bloombergController.TicketTouched += TicketTouched;
            _bloombergController.CancelTouch = false;

            textBoxWindowRegEx.Text = Settings.WindowTitleRegEx;

            listViewTickets.Columns.Add("Tickets");
            ResizeTicketListView(0);

            EnableDisableControls();
        }

        private void ButtonFindWindow_Click(object sender, EventArgs e)
        {
            foundWindow = false;
            textBoxWindowPid.Text = string.Empty;
            textBoxWindowTitle.Text = string.Empty;

            this.Refresh();

            RemoteWindow = new RemoteWindow(textBoxWindowRegEx.Text);
            if (RemoteWindow.RemoteProcess != null)
            {
                textBoxWindowPid.Text = $"{RemoteWindow.RemoteProcess.Id}";
                textBoxWindowTitle.Text = RemoteWindow.RemoteProcess.MainWindowTitle;
                foundWindow = true;
            }
            else
            {
                MessageBox.Show($"Unable to find \"{textBoxWindowRegEx.Text}\"", "Not Found");
            }

            EnableDisableControls();
        }

        private void ButtonActivateWindow_Click(object sender, EventArgs e)
        {

            if (!_bloombergController.ActivateRemote(stayOnRemote: false))
            {
                MessageBox.Show($"Unable to activate window, '{RemoteWindow.RemoteProcess.MainWindowTitle}', Pid={RemoteWindow.RemoteProcess.Id}", "Error");
            }

            EnableDisableControls();
        }

        private void ButtonGetTickets_Click(object sender, EventArgs e)
        {
            ClearTickets();

            this.Cursor = Cursors.WaitCursor;
            try
            {
                Tickets = _bloombergController.GetTicketsToTouch();
                foreach (var ticket in Tickets)
                {
                    ListViewItem lvi = new ListViewItem(ticket);
                    listViewTickets.Items.Add(lvi);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }

            ResizeTicketListView(Tickets.Count);

            this.Cursor = Cursors.Default;
            EnableDisableControls();
        }

        private void ButtonTouchTickets_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            _bloombergController.SendTickets();

            this.Cursor = Cursors.Default;
        }

        private void ButtonClearTickets_Click(object sender, EventArgs e)
        {
            ClearTickets();

            ResizeTicketListView(0);

            EnableDisableControls();
        }

        private void TicketTouched(object sender, TicketTouchedEventArgs e)
        {
            DialogResult result = MessageBox.Show($"Ticket {e.TicketNumber} touched.  Continue?", "Ticket Touched", MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
            {
                //do something to stop processing
                _bloombergController.CancelTouch = true;
            }
        }
        


        #region Helper Methods

        private void ResizeTicketListView(int ticketCount)
        {
            const int minHeight = 20;
            const int _maxHeight = 400;

            listViewTickets.Size = new System.Drawing.Size(
                listViewTickets.Size.Width,
                Math.Max(
                    minHeight,
                    Math.Min(
                        _maxHeight,
                        ticketCount * 20 + 10
                    )
                )
            );

        }

        private void EnableDisableControls()
        {
            buttonActivateWindow.Enabled = false;
            buttonTouchTickets.Enabled = false;
            buttonClearTickets.Enabled = false;

            if (foundWindow)
                buttonActivateWindow.Enabled = true;

            if (foundWindow && listViewTickets.Items.Count != 0)
                buttonTouchTickets.Enabled = true;

            if (listViewTickets.Items.Count != 0)
                buttonClearTickets.Enabled = true;

        }

        private void ClearTickets()
        {
            Tickets.RemoveAll(x => true);
            listViewTickets.Items.Clear();
        }

        #endregion

    }
}