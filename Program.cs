﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Extensions.Logging;

using PTrust.EnterpriseLogger.IntegrationExtensions;
using PTrust.EnterpriseLogger.Messages;


namespace BloombergTicketToucher
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ConfigureLogger();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        private static void ConfigureLogger()
        {
            var loggerFactory = new LoggerFactory().AddEnterpriseLog();
            loggerFactory.ConfigurePtLogger<Program>(ProductStackType.TradingSystem);
            PtLogger.LogInfo("Started Bloomberg Ticket Toucher ");
        }

    }
}
