﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BloombergTicketToucher.Interfaces;
using RestSharp;
using RestSharp.Authenticators;

using BloombergTicketToucher.Contracts;
using BloombergTicketToucher.REST;
using PTrust.EnterpriseLogger.IntegrationExtensions;
using TradeControl.Domain.DataContracts.Types.Reporting.CouponRecon;

namespace BloombergTicketToucher.Query
{
    class TradeControlQueryService : ITradeControlQueryService
    {
        private int _timeout;
        private string _tradeControlUrl;
        public TradeControlQueryService()
        {
            _timeout = Settings.RestTimeoutInSeconds;
            _tradeControlUrl = Settings.TradeControlUrl;
        }

        public List<string> GetTicketsToTouch()
        {
            //for now pretending results aren't page.  Just asking for 200 which may be more then we ever have
            PtLogger.LogInfo($"Get Tickets to Touch from TradeControl ({_tradeControlUrl}) CouponRecon endpoint ({URL.CouponRecon}) with {_timeout} second timeout.");

            RestClient restClient = new RestClient(_tradeControlUrl)
            {
                Timeout = _timeout * 1000,
                Authenticator = new NtlmAuthenticator()
            };

            RestRequest tradeControlRequest;
            tradeControlRequest = new RestRequest(URL.CouponRecon, Method.POST);

            DateTimeOffset today = DateTime.SpecifyKind(DateTime.Today, DateTimeKind.Local).AddMonths(-1);
            DateTimeOffset firstOfMonth = today.AddDays(-today.Day + 1);
            DateTimeOffset lastOfMonth = firstOfMonth.AddMonths(1).AddDays(-1);

            PtLogger.LogInfo($"Parameters:  StartDate={firstOfMonth:yyyy-MM-dd}, EndDate={lastOfMonth:yyyy-MM-dd}");
            CouponReconUserRequestModel requestBody = new CouponReconUserRequestModel()
            {
                StartDate = firstOfMonth,
                EndDate = lastOfMonth,
                TicketId = null,
                SecurityId = string.Empty,
                SortColumn = string.Empty,
                SortDescending = true,
                PageNumber = 0,
                RecordsPerPage = 200,
                AccruedDiff100 = true,
                AccruedDiffFlat = false,
                AccruedAlloDiff100 = true,
                AccruedAlloDiffFlat = false,
                AccruedDiffNull = false,
                AccruedAlloDiffNull = false
            };

            tradeControlRequest.AddJsonBody(requestBody);

            //tradeControlRequest.AddJsonBody
            var esmResult = restClient.Post<SecurityTicketQueryResponse>(tradeControlRequest);

            if (esmResult.IsSuccess())
            {
                var toReturn = esmResult.Data.Tickets.Select(x => $"{x.BloombergTransactionNumber}").OrderBy(x => x)
                    .ToList();

                PtLogger.LogInfo($"Found {toReturn.Count} tickets");

                return toReturn;
            }
            else
            {
                string errorMessage = "Error getting Tickets to Touch";
                PtLogger.LogError(errorMessage);
                throw new Exception(errorMessage);
            }
        }

    }
}
